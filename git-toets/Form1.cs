﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace git_toets
{
    public partial class Form1 : Form
    {
        private float _width, _height;
        private float _surface;
        private float _outlineMeter;
        private bool _onderhoud;
        public Form1()
        {
            InitializeComponent();
        }

        private void UpdateCalculations()
        {
            string textBoxTuin = textBox2.Text; //m2
            string textBoxHout = textBox1.Text;  //m

            float result = 0f;
            string tuinType = string.Empty;
            string houtType = string.Empty;

            if (Single.TryParse(textBoxTuin, out _surface))
                Console.WriteLine(_surface);
            else
                Console.WriteLine("Unable to parse '{0}'.", textBoxTuin);

            if (Single.TryParse(textBoxHout, out _outlineMeter))
                Console.WriteLine(_outlineMeter);
            else
                Console.WriteLine("Unable to parse '{0}'.", textBoxHout);

            try
            {
                tuinType = comboBox1.SelectedItem.ToString();
            }
            catch (NullReferenceException) { }
            try
            {
                houtType = comboBox2.SelectedItem.ToString();
            }
            catch (NullReferenceException) { }
            
            //debug
            //MessageBox.Show(tuinType + " " + houtType + " " + _surface + " " + _outlineMeter + " " + _onderhoud);

            switch(tuinType)
            {
                case "Standaard € 30":
                    result = result + (30 * _surface);
                    break;
                case "Europees € 45":
                    result = result + (45 * _surface);
                    break;
                case "Aziatisch € 80":
                    result = result + (80 * _surface);
                    break;
                case "Extra € 100":
                    result = result + (100 * _surface);
                    break;
                default:
                    break;
            }

            switch (houtType)
            {
                case "Grenenhout € 15":
                    result = result + (15 * _outlineMeter);
                    break;
                case "Vurenhout € 25":
                    result = result + (25 * _outlineMeter);
                    break;
                default:
                    break;
            }

            if (_onderhoud)
            {
                result += (_surface / 32) * 45;
            }

            result = result + 500;

            totaaleuro.Text = result.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCalculations();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCalculations();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            UpdateCalculations();
        }

        private void checkbox_CheckedChanged(object sender, EventArgs e)
        {
            _onderhoud = checkbox.Checked;
            UpdateCalculations();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateCalculations();
        }
    }
}
